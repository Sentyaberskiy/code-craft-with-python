### Опис проблеми чи питання
<!--- Підсумуйте, що ви хочете обговорити: -->
<!--- які питання у вас виникли, як себе поводить програма і т.д. -->


### Додаткова інформація: логі, скріншоти
<!--- Paste any relevant logs - please use code blocks (```) to format -->
<!--- console output, logs, and code as it's very hard to read otherwise -->


/label ~discussion
